import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

import java.util.Random;

public class Main {

    public static void main(String[] args) throws SerialPortException {
        String[] portNames = SerialPortList.getPortNames();
        for (String portName : portNames) {
            System.out.println(portName);
        }
        for (int i = 0; i < 5; i++) {
            write("COM9", getRandomText());
            read("COM8");
        }

    }

    public static void write(String portName, byte[] data) {
        SerialPort serialPort = new SerialPort(portName);
        try {
            serialPort.openPort();
            serialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            serialPort.writeBytes(data);
        } catch (SerialPortException e) {
            e.printStackTrace();
        } finally {
            try {
                serialPort.closePort();
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    public static void read(String portName) {
        SerialPort serialPort = new SerialPort(portName);
        try {
            serialPort.openPort();
            serialPort.setParams(9600, 8, 1, 0);

            byte[] buffer = serialPort.readBytes();
            if (buffer != null) {
                System.out.println(new String(buffer));
            }
//            serialPort.addEventListener(e -> {
//                if(e.isRXCHAR()){
//                    if(e.getEventValue() >= 10){
//                        try {
//                            byte buffer[] = serialPort.readBytes(10);
//                            System.out.println(new String(buffer));
//                        }
//                        catch (SerialPortException ex) {
//                            ex.printStackTrace();
//                        }
//                    }
//                }
//            });
        } catch (SerialPortException e) {
            e.printStackTrace();
        } finally {
            try {
                serialPort.closePort();
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    public static byte[] getRandomText() {
        Random random = new Random();
        byte[] bytes = new byte[2];
        random.nextBytes(bytes);
        return bytes;
    }
}
